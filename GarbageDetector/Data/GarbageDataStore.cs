﻿using Firebase.Database;
using Firebase.Database.Query;
using MvvmHelpers;
using GarbageDetector.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageDetector.Data
{
    public class GarbageDataStore : IDataStore<Garbage>
    {
        public List<Garbage> items { get; set; } 
        private FirebaseClient firebaseClient = new FirebaseClient("https://xamarin-pokedex-default-rtdb.europe-west1.firebasedatabase.app/");

        public GarbageDataStore()
        {
            items = new List<Garbage>();
        }
        public async Task<bool> AddItemAsync(Garbage item)
        {
            await firebaseClient.Child("Garbage").PostAsync( new Garbage 
            {
                Name = item.Name,
                TimeToDecompose = item.TimeToDecompose,
                ImageName = item.ImageName,
            });
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string name)
        {
            var oldItem = items.Where((Garbage arg) => arg.Name == name).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Garbage> GetItemAsync(string name)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Name == name));
        }

        public async Task<IEnumerable<Garbage>> GetItemsAsync(bool forceRefresh = false)
        {
            List<Garbage> garbage = (
                await firebaseClient
                .Child("Garbage")
                .OnceAsync<Garbage>())
                .Select(item => new Garbage
                {
                    Name = item.Object.Name,
                    TimeToDecompose = item.Object.TimeToDecompose,
                    ImageName = item.Object.ImageName,
                }).ToList();

            return await Task.FromResult(garbage);
        }

        public async Task<bool> UpdateItemAsync(Garbage item)
        {
            var oldItem = items.Where((Garbage arg) => arg.Name == item.Name).FirstOrDefault();
            int oldIndex = items.IndexOf(oldItem);
            items.Remove(oldItem);
            items.Insert(oldIndex, item);

            return await Task.FromResult(true);
        }
    }
}
