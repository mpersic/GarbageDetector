﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using GarbageDetector.Models;
using Newtonsoft.Json;
using Plugin.Toast; 

namespace GarbageDetector
{
    public class OnlineClassifier : IClassifier
    {
        public event EventHandler<ClassificationEventArgs> ClassificationCompleted;

        public async Task Classify(byte[] bytes)
        {
            var client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 10);

            client.DefaultRequestHeaders.Add("Prediction-Key", "a8b18eeda8ad4794b56331293001b2b7");

            string url = "https://pokedexinstanceferit.cognitiveservices.azure.com/customvision/v3.0/Prediction/72ca56ea-c89f-46a6-b16a-a5bba87f9256/classify/iterations/Iteration1/image";
            
            HttpResponseMessage response;

            using (var content = new ByteArrayContent(bytes))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = await client.PostAsync(url, content);
                var json = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<PredictionResult>(json);

                ClassificationCompleted.Invoke(this, new ClassificationEventArgs(result.Predictions));
            }
        }
    }
}
