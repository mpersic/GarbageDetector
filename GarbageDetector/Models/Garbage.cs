﻿using GarbageDetector.ViewModels;
using System;
namespace GarbageDetector.Models
{
    public class Garbage
    {
        public string Name { get; set; }
        public string TimeToDecompose { get; set; }
        public string ImageName { get; set; }
    }
}
