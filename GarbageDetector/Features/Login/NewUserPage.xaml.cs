﻿
using Xamarin.Forms;

namespace GarbageDetector.Features.Login
{
    public partial class NewUserPage : ContentPage
    {
        public NewUserPage()
        {
            InitializeComponent();

            BindingContext = new NewUserViewModel();
        }
    }
}
