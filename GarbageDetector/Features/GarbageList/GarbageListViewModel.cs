﻿using MvvmHelpers;
using GarbageDetector.Contracts;
using GarbageDetector.Data;
using GarbageDetector.Models;
using GarbageDetector.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TinyMvvm;
using Xamarin.Forms;
using GarbageDetector.Features.Shell;
using GarbageDetector.Features.Login;

namespace GarbageDetector.ViewModels
{
    public class GarbageListViewModel : BaseViewModel
    {
        public GarbageListViewModel(IDataStore<Garbage> dataStore)
        {
            Garbage = new ObservableRangeCollection<Garbage>();
            this.DataStore = dataStore;
            LoadItemsCommand = new Command(async () => await LoadItems());
            SignOutCommand = new Command(OnSignOut);
        }

        #region Commands

        public Command LoadItemsCommand { get; }
        public Command SignOutCommand { get; }
        #endregion
        public ObservableRangeCollection<Garbage> Garbage { get; set; }
        public IDataStore<Garbage> DataStore { get; }

        public async void OnAppearing()
        {
            await LoadItems();
        }

        public async Task LoadItems()
        {
            IsBusy = true;

            //await AddItems();          

            try
            {
                Garbage.Clear();
                var items = await DataStore.GetItemsAsync(true);
                Garbage.ReplaceRange(items);
            }
            catch (Exception ex)
            {
                ToastProvider.LongAlert("Something went wrong when loading garbage");
            }
            finally
            {
                IsBusy = false;
            }

        }

        private async Task AddItems()
        {
            await DataStore.AddItemAsync(new Garbage { Name = "Name: Cardboard", TimeToDecompose = "TimeToDecompose: 2 months", ImageName = "cardboard.png"});
            await DataStore.AddItemAsync(new Garbage { Name = "Name: Glass", TimeToDecompose = "TimeToDecompose: a million years", ImageName = "glass.png"});
            await DataStore.AddItemAsync(new Garbage { Name = "Name: Metal", TimeToDecompose = "TimeToDecompose:  80-100 years", ImageName = "metal.png"});
            await DataStore.AddItemAsync(new Garbage { Name = "Name: Paper", TimeToDecompose = "TimeToDecompose: two to six weeks", ImageName = "paper.png"});
            await DataStore.AddItemAsync(new Garbage { Name = "Name: Plastic", TimeToDecompose = "TimeToDecompose: up to 1,000 years", ImageName = "plastic.png"});
            await DataStore.AddItemAsync(new Garbage { Name = "Name: Trash", TimeToDecompose = "TimeToDecompose: ", ImageName = "trash.png"});

        }

        private void OnSignOut()
        {
            try
            {
                var authService = DependencyService.Resolve<IAuthenticationService>();
                authService.SignOut();
                Application.Current.MainPage = new LoginShell();
            }
            catch (Exception ex)
            {
                ToastProvider.LongAlert("Something went wrong when signing out!");
            }
        }

    }
}
