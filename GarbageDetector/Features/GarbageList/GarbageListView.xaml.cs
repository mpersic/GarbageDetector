﻿using GarbageDetector.Data;
using GarbageDetector.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GarbageDetector.Features.Shell;

namespace GarbageDetector.Views
{

    public partial class GarbageListView: ContentPage
    {
        public GarbageListViewModel _viewModel;
        public GarbageListView()
        {
            InitializeComponent();
            BindingContext = _viewModel = new GarbageListViewModel(new GarbageDataStore());

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
