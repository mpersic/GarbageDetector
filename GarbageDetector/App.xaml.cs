﻿using GarbageDetector.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GarbageDetector.Features.Login;
using GarbageDetector.Features.Shell;

namespace GarbageDetector
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Bootstrapper.Init(this);

            MainPage = new LoginShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
